<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


   Route::get('/', function () {

	if(Auth::check()){
		

		return redirect()-> action('HomeController@index');
	}
	else{
		  return view('welcome');
	}
});

Auth::routes();

Route::get('/home', 'PostsController@index')->name('home');



Route::group(['middleware' => 'auth'], function () {
    Route::get('/users', 'FollowController@index');
    Route::post('/follow/{user}', 'FollowController@follow');
    Route::delete('/unfollow/{user}', 'FollowController@unfollow');
    Route::get('/feed', 'FeedsController@newsFeed');
    Route::resource('posts', 'PostsController');
      Route::get('/notifications', 'FeedsController@notification');
});
